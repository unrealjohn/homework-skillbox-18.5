// Homework_Skillbox_18.5.cpp : This file contains the 'main' function. Program execution begins and ends there.

#include <iostream>

class Stack
{
private:
    int LastMember = NULL;
    int Size = 10000000000;
    int* Pot = new int[Size];

public:
    int show()
    {
        std::cout << "Size of Stack: "<< LastMember << ". ";
        std::cout << "Stack: ";

        for (int i = 0; i < LastMember; i++)
        {
            std::cout << *(Pot + i) << ' ';
        }
        std::cout << "\n";
        return LastMember;
    }
    
    void push(int var)
    {
        Pot[LastMember] = var;
        std::cout << "Pushed: " << var << "\n";
        LastMember++;
    }

    int pop()
    {
        int var = Pot[LastMember-1];
        std::cout << "Poped: " << var << "\n";
        LastMember--;

        return var;
    }

    void main()
    {
    delete[] Pot;
    }
};

int main()
{
    Stack a;
    a.show();
    a.push(100);
    a.push(200);
    a.push(300);
    a.push(400);
    a.show();
    a.pop();
    a.pop();
    a.show();
    a.push(666);
    a.push(999);
    a.show();
}




/*
void main()
{
    int size = 5;
    int* arr = new int[size];
    for (int i = 0; i < size; i++)
    {
    cout << arr[i] << " " << endl;
    }
    delete[] arr;
}
*/


/*
int main()
{
	int x;
	std::cin >> x;
	int* p = new int[x];
	delete p;
	p = nullptr;
	std::cout << "Hello World!\n";
}
*/


